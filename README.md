This is a implementation proposal to solve the HTTP TicTacToe exercise described here : 


Run the projet with your IDE. The project is configured to use Maven in order to install dependencies. Install with `mvn install` or throught your IDE.


The integration test suite https://gitlab.com/simplon-grenoble-cda/semaine-1-poo/formateur/poo-exercices/exhttptictactoe here runs correctly with this implementation.



# Contexte du projet

Ce projet sert deux fonctions dans le cadre de la formation

D'une part il sert de solution à l'exercice proposé ici : https://gitlab.com/simplon-grenoble-cda/semaine-1-poo/formateur/poo-exercices/exhttptictactoe
Il s'agit d'un jeu de morpion (Tic Tac Toe), servi via un serveur HTTP.

D'autre part, la partie modèle du code (le package `game`) sert de base pour un exercice qui consiste à implémenter une version en ligne de commande du jeu
de morpion, en essayant de suivre un modèle MVC d'organisation du code.



# Branches

- `master` : implémentation du jeu de morpion sur HTTP
- `prepare-mvc` : base du code pour commencer l'implémentation en ligne de commande, avec principe MVC
- `mvc-step1-solution` : début de l'implémentation cli avec MVC

# Étapes pour implémenter le morpion CLI (principe MVC)

## 1 - Implémenter la phase où l'on demande le nom des joueurs

### Modèle

Une partie est représentée par la classe `Game`, qui est associée à la classe `Player` deux fois, pour représenter son joueur A et son joueur B.

L'objectif de l'implémentation est d'arriver à avoir notre instance de `Game` associée à deux instances `Player` qui auront été créées,
avec le bon attribut `name` conformément à ce que l'utilisateur aura entré.

### Controller

Le controleur a la responsabilité de manipuler le modèle. Pour arriver à notre but (création et association des joueurs), nous allons
créer une méthode `setPlayers(String playerAName, String playerBName)` dans notre controleur `GameController`.

### Vue

Le but de la vue est de provoquer l'affichage  à la console de l'invite qui propose aux joueurs de rentrer leurs noms, puis de transmettre
les données obtenues au controleur.

On a choisi d'implémenter la vue en deux classes séparées : 
- `GameViewRunner` qui s'occupe de la logique de l'enchainement des affichages et de faire les liens avec le controleur,
- `GameViewRenderer` qui s'occupe de faire les affichages à la console

On implémente les méthodes `GameViewRunner.promptPlayers()` et `GameViewRenderer.renderPromptPlayer()`.

- `promptPlayers()` : responsable de l'exécution du jeu pour la partie qui consiste à demander aux deux joueurs leurs noms, et
  faire stocker ces noms dans le modèles (via un appel au controlleur)
- `renderPromptPlayer()` : affiche a la console un invite de commande qui demande le nom d'un joueur, et renvoie la valeur 
  rentrée par ce joueur


Les diagrammes de séquence ci dessous décrivent l'articulation de ces méthodes.

![](./imgs/TicTacToe_Model.png)

![](./imgs/TicTacToe_Sequence1.png)

![](./imgs/TicTacToe_Sequence2.png)


## 2 - Implémenter la boucle qui permet de jouer des coups

### Vue

Compléter l'implémentation de `runSingleGame()` : ajouter une boucle pour faire : 

- demander un coup à jouer au joueur dont c'est le tour
- mettre à jour la grille avec le coup joué

(pour l'instant ne pas gérer les exceptions renvoyées par le modèle en cas de mauvaise entrée)

Pour faire l'affichage, on va s'appuyer sur des méthodes à implémenter dans `GameViewRenderer` : 

- `promptMoveToPlay(Player player)` : afficher un message d'invite sur la console pour proposer au joueur dont c'est le tour
  d'indiquer la case sur laquelle il veut jouer. Récupérer la valeur entrée, valider que la valeur est correcte (au sens du format attendu, pas 
  au sens de la logique du jeu), et renvoyer la valeur de la case à jouer.

- `renderGrid(PlayerToken[] gridCells)`  : affiche l'état de la grille

### Controller

Pour modifier l'état du jeu, on va utiliser et implémenter la méthode du controleur `GameController.playRound(PlayerToken player, int cellIdx)`,
cette méthode va utiliser le modèle et ses méthodes pour mettre à jour le jeu.


## 3 - Prévoir la sortie du jeu

### Controller

Pour prévoir la sortie du jeu, on va avoir besoin de méthodes qui nous indiquent si le jeu est terminé (grille pleine et/ou vainqueur),
donc implémenter des méthodes du controleur `Game.isGameFinished()`, `Game.getWinner()`.

- `isGameFinished()` : renvoit un booléen, `true` si le jeu est terminé, `false` sinon
- `getWinner()` renvoit `PlayerToken.A` ou `PlayerToken.B` si un de ces joueurs est vainqueur, sinon renvoit null

### Vue

Complémenter l'implémentation de `runSingleGame()` en s'appuyant sur les nouvelles méthodes du controleur pour gérer les conditions
de sortie de la boucle du jeu. Déclencher les affichages en fonction du scénario (match nul ou victoire).

Pour les affichages, implémenter des nouvelles méthodes sur `GameViewRender`, par exemple : 

- `GameViewRenderer.renderVictory(Player player)` : affiche le message de victoire du joueur 
- `GameViewRenderer.renderDraw()` : affiche un message de match nul


## 4 - Gérer les exceptions

Quand on place des tokens sur la grille, deux exceptions peuvent se produire `BadPlayerException`, et `CellNotEmptyException`.

Gérer ces exceptions dans le code de `GameViewRunner`. 

## 5 - Gérer l'affichage d'une nouvelle partie

Changer l'implémentation de `GameViewRunner.promptPlayAgain()` pour permettre à l'utilisateur de jouer une nouvelle partie.
Continuer à suivre la logique mise en œuvre jusqu'à maintenant.

# Aller plus loin : mettre en place des interface Java

Utiliser le mécanisme POO des interfaces, pour permettre plus de flexibilité dans notre code.

Créer une interface `GameViewRendererInterface`, qui reprend la signature de la classe `GameViewRenderer`.

Transformer `GameViewRenderer` en `EnglishGameViewRenderer` qui implémente l'interface `GameViewRendererInterface`.

Créer une autre classe `FrenchGameViewRenderer` qui implémente aussi l'interface `GameViewRendererInterface`.

Voir que le mécanisme d'interface permet de changer facilement d'une implémentation à l'autre.
