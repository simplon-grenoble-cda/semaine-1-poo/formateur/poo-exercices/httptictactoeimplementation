package com.company.game;

/**
 * Represent the player A or B of a TicTacToe game
 */
public enum PlayerToken {
    /**
     * Value for a cell played by player A
     */
    A,
    /**
     * Value for a cell played by player B
     */
    B
}