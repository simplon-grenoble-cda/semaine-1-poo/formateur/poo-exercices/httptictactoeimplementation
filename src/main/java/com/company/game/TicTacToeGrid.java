package com.company.game;

import java.util.*;

/**
 * This class represents a the grid of a Tic Tac Toe game. It stores
 * the content of the grid (which cells are played).
 */
public class TicTacToeGrid {

    /**
     * This array represent the tokens played in the grid. It has 9 elements,
     * representing the 9 cells of the grid, from top left, to bottom right
     * (browsing the cells horizontally).
     *
     * Empty cells are null. Played cells are PlayerToken.A or PlayerToken.B
     */
    private PlayerToken[] grid;

    public TicTacToeGrid() {
        this.grid = new PlayerToken[9];
    }

    /**
     * @return Current representation of the grid
     */
    public PlayerToken[] getGrid() {
        return Arrays.copyOf(this.grid, 9);
    }



    /**
     * Plays a token on the tic tac toe grid
     *
     * @param gridIdx The index of the cell on which the token must be played
     * @param playerToken Indicates if we play a token for player A or player B
     * @throws CellNotEmptyException When attempting to play a token on a non empty cell
     */
    public void addToken(int gridIdx, PlayerToken playerToken) throws CellNotEmptyException {
        if (this.grid[gridIdx] != null) {
            throw new CellNotEmptyException();
        } else {
            this.grid[gridIdx] = playerToken;
        }
    }

    /**
     * If the grid has a winner, returns the PlayerToken value representing
     * that winner, otherwise returns null.
     * @return  - PlayerToken.A if A is the winner
     *          - PlayerToken.B if B is the winner
     *          - null if there is no winner
     */
    public PlayerToken getWinner() {
        PlayerToken winner;
        if ((winner = this.checkVictoryRows()) != null) {
            return winner;
        } else if ((winner = this.checkVictoryColumns()) != null) {
            return winner;
        } else if ((winner = this.checkVictoryDiagonals()) != null) {
            return winner;
        } else {
            return null;
        }
    }

    /**
     * Helper method, check if there is a winner on the grid columns. Returns null if no
     * winner found.
     */
    private PlayerToken checkVictoryColumns() {
        for (int columnId = 0; columnId < 3; columnId++) {
            if (this.grid[columnId] == this.grid[columnId + 3] && this.grid[columnId] == this.grid[columnId + 6]) {
                return this.grid[columnId];
            }
        }
        return null;
    }

    /**
     * Helper method, check if there is a winner on the grid columns. Returns null if no
     * winner found.
     */
    private PlayerToken checkVictoryRows() {
        for (int rowId = 0; rowId < 3; rowId++) {
            int rowBaseIdx = rowId*3;
            if (this.grid[rowBaseIdx] == this.grid[rowBaseIdx + 1] && this.grid[rowBaseIdx] == this.grid[rowBaseIdx + 2]) {
                return this.grid[rowBaseIdx];
            }
        }
        return null;
    }

    /**
     * Helper method, check if there is a winner on the grid diagonals. Returns null if no
     * winner found.
     */
    private PlayerToken checkVictoryDiagonals() {
        if (this.grid[0] == this.grid[4] && this.grid[0] == this.grid[8]) {
            return this.grid[0];
        } else if (this.grid[2] == this.grid[4] && this.grid[2] == this.grid[6]) {
            return this.grid[2];
        } else {
            return null;
        }
    }

}
