package com.company.server;

import com.company.game.BadPlayerException;
import com.company.game.CellNotEmptyException;
import com.company.game.Game;
import com.company.game.PlayerToken;
import com.company.utilities.JSONGenerator;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class GamesHandler implements HttpHandler {
    private GamesManager gamesManager;

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        this.gamesManager = GamesManager.getGamesManagerInstance();

        String requestMethod = httpExchange.getRequestMethod();
        String query = httpExchange.getRequestURI().getPath();
        System.out.println(query);
        String[] splitUrl = query.split("/");


        if (splitUrl[2].equals("new")) {
            newGame(httpExchange);
        }

        if (isAInt(splitUrl[2])) {
            int id = Integer.parseInt(splitUrl[2]);
            if (requestMethod.equals("GET")) {
                gameStatus(httpExchange, id);
            }
            else if (requestMethod.equals("POST")) {
                playToken(httpExchange, id);
            }
        }
    }


    public void newGame(HttpExchange httpExchange) throws IOException {
        Game game = this.gamesManager.createNewGame();

        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("gameId", game.getId());

        HttpResponseHelpers.sendJSONResponse(httpExchange, 200, jsonResponse);
    }



    public void gameStatus(HttpExchange httpExchange, int gameId) throws IOException {
        Game game = this.loadGameInstance(httpExchange, gameId);

        if (game != null) {
            /** Send the game status */
            String responseBody = JSONGenerator.getGameStatusJSON(game);
            HttpResponseHelpers.sendJSONResponse(httpExchange, 200, responseBody);
        }
    }

    public void playToken(HttpExchange httpExchange, int gameId) throws IOException {
        /** Parse request body */
        String requestBody = IOUtils.toString(httpExchange.getRequestBody(), StandardCharsets.UTF_8);
        JSONObject requestJson = new JSONObject(requestBody);
        String playerParameter = requestJson.getString("player");
        int cellIdParameter = requestJson.getInt("cell");

        /** Get Game instance (and check that gameId is correct) */
        Game game = this.loadGameInstance(httpExchange, gameId);
        if (game == null) {
            return;
        }

        /** Check that the request parameters are correct */
        boolean playerParameterCorrect = playerParameter.equals("A") || playerParameter.equals("B");
        boolean cellIdParameterCorrect = cellIdParameter >= 0 && cellIdParameter < 9;
        if (!playerParameterCorrect || !cellIdParameterCorrect) {
            HttpResponseHelpers.sendResponseError(
                    httpExchange,
                    "bad_request_parameters",
                    "Les paramètres donnés à la requête ne sont pas corrects"
            );
            return;
        }

        /**
         * Play the move, send the success response with the game status,
         * and handle errors and send HTTP error response accordingly.
         */
        try {
            PlayerToken playerToken = playerParameter.equals("A") ? PlayerToken.A : PlayerToken.B;
            game.playToken(cellIdParameter, playerToken);
            String responseBody = JSONGenerator.getGameStatusJSON(game);
            HttpResponseHelpers.sendJSONResponse(httpExchange, 200, responseBody);
        } catch (CellNotEmptyException e) {
            HttpResponseHelpers.sendResponseError(
                    httpExchange,
                    "cell_not_empty",
                    "La case n'est pas vide"
            );
        } catch (BadPlayerException e) {
            HttpResponseHelpers.sendResponseError(
                    httpExchange,
                    "bad_player",
                    "Ce n'est pas à vous de jouer"
            );
        }
    }

    /**
     * Load the game instance from the given gameId.
     * If the gameId is invalid, sends a HTTP response with error 404 not found.
     *
     * @param httpExchange
     * @param gameId
     */
    public Game loadGameInstance(HttpExchange httpExchange, int gameId) throws IOException {
        Game game = this.gamesManager.getGame(gameId);

        if (game == null) {
            /** Handle case for game not found with the provided gameId */
            httpExchange.sendResponseHeaders(404, 0);
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.close();
        }
        return game;
    }

    public boolean isAInt(String chaine) {
        try {
            Integer.parseInt(chaine);
        } catch (NumberFormatException e){
            return false;
        }
        return true;
    }

}
