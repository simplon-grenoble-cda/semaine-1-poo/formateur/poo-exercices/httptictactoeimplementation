package com.company.server;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Main {

    /**
     * Entrypoint to our TicTacToe server
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) throws IOException {
        // Creating the server
        final int SERVER_PORT = 3000;
        HttpServer server = HttpServer.create(new InetSocketAddress(SERVER_PORT), 0);

        // Setup the endpoint /games/*
        server.createContext("/games", new GamesHandler());

        // launch the server
        server.start();
    }
}
