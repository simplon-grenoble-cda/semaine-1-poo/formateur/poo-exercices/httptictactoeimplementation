package com.company.utilities;

import com.company.game.Game;
import com.company.game.PlayerToken;
import org.json.JSONArray;
import org.json.JSONObject;

public class JSONGenerator {

    public static String getGameStatusJSON(Game game) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("gameId", game.getId());

        JSONArray jsonCellsArray = new JSONArray();
        PlayerToken[] cells = game.getGridCells();
        for (PlayerToken playerToken : cells) {
            jsonCellsArray.put(PlayerTokenToString(playerToken));
        }

        jsonObject.put("grid", jsonCellsArray);
        jsonObject.put("nextRound", game.getNextRoundPlayer() != null ? game.getNextRoundPlayer() : JSONObject.NULL);
        jsonObject.put("winner", game.getWinner() != null ? game.getWinner() : JSONObject.NULL);

        return jsonObject.toString();
    }

    /**
     * Represent a value of the enum {@link PlayerToken} into a String. Particularity :
     * a null value is represented by "0" (this is needed to represent the tic tac toe grid
     * as a JSON array per the API specification)
     *
     * @param playerToken
     * @return
     */
    public static String PlayerTokenToString(PlayerToken playerToken) {
        if (playerToken == null) {
            return "0";
        } else {
            switch(playerToken) {
                case A:
                    return "A";
                case B:
                    return "B";
                default:
                    return "0";
            }
        }
    }
}
